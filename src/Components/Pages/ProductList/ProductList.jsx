import React from "react";
import {Layout} from 'antd';
import './ProductList.css'

const {Content} = Layout;

class ProductList extends React.Component{
    render() {
        return (
            <Layout className="layout">
                <Content style={{ padding: '50px 50px' }}>
                    <div className="site-layout-content">Products</div>
                </Content>
            </Layout>
        )
    }
}

export default ProductList;
