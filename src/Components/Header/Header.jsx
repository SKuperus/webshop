import React from "react";
import {Layout, Menu} from "antd";
import './Header.css'
import Search from "antd/es/input/Search";
import Button from "antd/es/button";
import {ShoppingCartOutlined} from "@ant-design/icons";

const {Header} = Layout;

class WebHeader extends React.Component {
    render() {
        return (
            <Layout className="layout">
                <Header>
                    <a className={"logo"} href={"/"}>HERKER</a>
                    <Menu theme="dark" mode="horizontal">
                        <Search className="search" placeholder="Zoeken..." style={{width: 200}}/>
                        <Menu.Item><a href={"/"}>Home</a></Menu.Item>
                        <Menu.Item><a href={"/hairproducts"}>Haarproducten</a></Menu.Item>
                        <Menu.Item><a href={"/accessories"}>Accessoires</a></Menu.Item>
                        <Menu.Item><a href={"/sale"}>Sale</a></Menu.Item>
                        <Menu.Item><a href={"/aboutus"}>Over ons</a></Menu.Item>
                        <Button className="login" type="default" shape="round" size={"large"}>
                            <a href={"/login"}>
                                Login
                            </a>
                        </Button>
                        <a href={"/cart"}>
                            <ShoppingCartOutlined className="cart"/>
                        </a>
                    </Menu>
                </Header>
            </Layout>
        )
    }
}

export default WebHeader;
