import React, { Component } from "react";
import "./App.css";
import Header from "./Components/Header/Header";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import 'antd/dist/antd.css'
import ProductList from "./Components/Pages/ProductList/ProductList";
import Footer from "./Components/Footer/Footer";
import Login from "./Components/Login/Login";
import AboutUs from "./Components/Pages/AboutUs/AboutUs";
import Cart from "./Components/Pages/Cart/Cart"

class App extends Component {
    render() {
        return (
            <Router>
            <div className="app">
                <Header/>
                <div className="app-body">
                    <div className="content">
                        <Switch>
                            <Route path="/" exact component={ProductList}/>
                            <Route path="/login" component={Login}/>
                            <Route path="/aboutus" component={AboutUs}/>
                            <Route path="/cart" component={Cart}/>
                            <Route path="/hairproducts" component={""}/>
                            <Route path="/accessories" component={""}/>
                            <Route path="/sale" component={""}/>
                            <Route
                                component={() => (
                                    <div style={{ padding: 20 }}>Page not found</div>
                                )}
                            />
                        </Switch>
                    </div>
                </div>
                <Footer/>
            </div>
            </Router>
        );
    }
}

export default App;
