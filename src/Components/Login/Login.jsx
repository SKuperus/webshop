import React from "react";
import Auth from "../../Auth";
import Input from "antd/es/input";
import {Button, Layout} from "antd";
import "./Login.css";
import Redirect from "react-router-dom/es/Redirect";
import withRouter from "react-router-dom/es/withRouter";

const {Content} = Layout;

class ConnectedLogin extends React.Component {

    state = {
        userName: "",
        pass: "",
        redirectToReferrer: false
    };

    render() {
        const {from} = this.props.location.state || {from: {pathname: "/"}};

        if (this.state.redirectToReferrer === true) {
            return <Redirect to={from}/>;
        }

        return (
            <Layout className="layout">
                <Content style={{padding: '50px 50px'}}>
                    <div className="div1">
                        <div className="div2">
                            <div className="div3">
                                {" "}
                                Log in{" "}
                            </div>
                            <Input value={this.state.userName}
                                   placeholder="Gebruikersnaam"
                                   onChange={e => {
                                       this.setState({userName: e.target.value});
                                   }}/>
                            <Button type={"primary"} style={{marginTop: 20, width: 200}}
                                    onClick={() => {
                                        // Simulate authentication call
                                        Auth.authenticate(this.state.userName, this.state.pass, user => {
                                            if (!user) {
                                                this.setState({wrongCred: true});
                                                return;
                                            }

                                            // this.props.dispatch(setLoggedInUser({ name: user.name }));
                                            this.setState(() => ({
                                                redirectToReferrer: true
                                            }));
                                        });
                                    }}>
                                Log in
                            </Button>
                        </div>
                    </div>
                </Content>
            </Layout>
        )
    }
}

const Login = withRouter(ConnectedLogin);

export default Login;
