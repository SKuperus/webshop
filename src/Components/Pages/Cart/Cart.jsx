import React from "react";
import {Layout} from 'antd';

const {Content} = Layout;

class Cart extends React.Component{
    render() {
        return (
            <Layout className="layout">
                <Content style={{ padding: '50px 50px' }}>
                    <div className="site-layout-content">Cart</div>
                </Content>
            </Layout>
        )
    }
}

export default Cart;
