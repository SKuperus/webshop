import React from "react";
import {Layout} from 'antd';

const {Footer} = Layout;

class WebFooter extends React.Component{
    render() {
        return (
            <Layout className="layout">
                <Footer style={{ textAlign: 'center' }}>HERKER © 2020 Simon Kuperus</Footer>
            </Layout>
        )
    }
}

export default WebFooter;
