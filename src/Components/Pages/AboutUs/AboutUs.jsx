import React from "react";
import {Layout} from "antd";

const {Content} = Layout;

class AboutUs extends React.Component{
    render() {
        return (
            <Layout className="layout">
                <Content style={{ padding: '50px 50px' }}>
                    <div className="site-layout-content">About us</div>
                </Content>
            </Layout>
        )
    }
}

export default AboutUs;
